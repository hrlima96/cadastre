class CreatePacients < ActiveRecord::Migration
  def change
    create_table :pacients do |t|
      t.string :name
      t.date :birth
      t.integer :dependents
      t.string :cpf


      t.timestamps null: false
    end
  end
end
