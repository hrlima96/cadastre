require 'spec_helper'
require 'faker'
require 'cpf_faker'
require 'selenium-webdriver'
include Warden::Test::Helpers

feature 'Pacient management' do

  before do
    @user = create(:user)
    @user.skip_confirmation!
    @user.save
    login_as @user

    unless example.metadata[:skip_before]
      @pacient = create(:pacient, user_id: @user.id)
      visit root_path
    end
  end

  scenario "adds a new pacient", skip_before: true do
    visit root_path

    expect{
      click_link 'Cadastrar paciente'
      fill_in 'Nome', with: 'Hugo Rodrigues de Lima'
      fill_in 'Data de Nascimento', with: '02/05/1996'
      fill_in 'Numero de Dependentes', with: '2'
      fill_in 'CPF', with: '051.672.433-94'
      click_button 'Cadastrar'
    }.to change(Pacient, :count).by(1)
  end

  scenario "show pacient" do
    click_link @pacient.name

    within 'h1' do
      expect(page).to have_content @pacient.name
    end
  end

  scenario "edit pacient" do
    click_link 'Editar'

    within 'h1' do
      expect(page).to have_content 'Editar Paciente'
    end

    expect(page).to have_selector("input[value=#{@pacient.name}]")

    nome = Faker::Name.last_name

    fill_in 'Nome', with: nome
    click_button 'Cadastrar'

    expect(page).to have_content nome
    expect(page).to have_content 'Paciente atualizado com sucesso.'
  end

  scenario "delete pacient" do
    expect{
      click_link 'Apagar'
    }.to change(Pacient, :count).by(-1)
  end

end