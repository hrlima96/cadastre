require 'spec_helper'

describe User do
  before do
    @user = create(:user)
  end

  it "sends a email from the correct sender" do 
    expect(open_last_email).to be_delivered_from 'hrlima96@gmail.com'
  end

  it "the receiver is correct" do
    expect(open_last_email).to be_delivered_to @user.email
  end
end