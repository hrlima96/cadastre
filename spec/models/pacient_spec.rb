require 'spec_helper'

describe Pacient do

  it "has a valid factory" do 
    expect(build(:pacient)).to be_valid
  end

  context "matching valid fields" do 
    it "is valid with a name, birth, dependents and cpf" do 
      #expect(@hugo).to be_valid
    end
  end

  context "matching invalid fields" do
    it "is invalid without a name" do
      pac = build(:pacient, name: nil)
      expect(pac).to have(1).errors_on(:name)
    end

    it "is invalid without a birth" do 
      pac = build(:pacient, birth: nil)
      expect(pac).to have(1).errors_on(:birth)
    end

    it "is invalid without a dependents" do
        pac = build(:pacient, dependents: nil)
      expect(pac).to have(2).errors_on(:dependents)
    end

    it "is invalid with dependent number lesser than 0" do
      pac = build(:pacient, dependents: -1)
      expect(pac).to have(1).errors_on(:dependents)
    end

    it "is invalid with dependent number greater than 50" do
      pac = build(:pacient, dependents: 60)
      expect(pac).to have(1).errors_on(:dependents)
    end

    it "is invalid without a cpf" do
      pac = build(:pacient, cpf: nil)
      expect(pac).to have(1).errors_on(:cpf)
    end

    it "is invalid with a cpf length different of 11 without dots and hifen" do
      pac = build(:pacient, cpf: "051.672.433")
      expect(pac).to have(1).errors_on(:cpf)
    end

    it "is invalid with a cpf format that's not valid " do
       pac = build(:pacient, cpf: "051.672.433-95")
      expect(pac).to have(1).errors_on(:cpf)
    end

    it "is invalid with a duplicate cpf" do
      pac = create(:pacient, cpf: "051.672.433-94")
      expect(build(:pacient, cpf: "051.672.433-94")).to have(1).errors_on(:cpf)
    end

    it "is invalid without name, birth, dependents, cpf" do
      pac = build(:pacient, name: nil, birth: nil, dependents: nil, cpf: nil)
      expect(pac).to have(1).errors_on(:name)
      expect(pac).to have(1).errors_on(:birth)
      expect(pac).to have(2).errors_on(:dependents)
      expect(pac).to have(1).errors_on(:cpf)
    end
  end
end