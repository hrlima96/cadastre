require 'faker'
require 'cpf_faker'

FactoryGirl.define do
  factory :pacient do
    name { Faker::Name.last_name }
    birth "1990-01-01"
    dependents Random.rand * 50
    cpf { Faker::CPF.cpf }
    user_id 0
    picture { File.new "#{Rails.root}/spec/factories/oi.jpg" }

    factory :invalid_pacient do
      name nil
    end

  end
end