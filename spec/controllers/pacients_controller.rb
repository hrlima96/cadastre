require 'spec_helper'

describe PacientsController do
  include CarrierWave::Test::Matchers
  
  before do
    @user = create(:user)
    @user.skip_confirmation!
    @user.save
    sign_in @user
  end

  describe "GET #index" do 
    it "populates an array with all pacients" do
      p1 = create(:pacient, user_id: @user.id)
      p2 = create(:pacient, user_id: @user.id)
      get :index
      expect(assigns(:pacients)).to match_array([p1, p2])
    end

    it "renders the :index template" do
      get :index
      expect(response).to render_template :index
    end

  end

  describe "GET #show" do 
    it "assigns the requested pacient to @pacient" do
      pacient = create(:pacient, user_id: @user.id)
      get :show, id: pacient.id
      expect(assigns(:pacient)).to eq pacient
    end

    it "renders the :show template" do
      pacient = create(:pacient, user_id: @user.id)
      get :show, id: pacient.id
      expect(response).to render_template :show
    end
  end

  describe "GET #new" do
    it "assigns a new pacient to @pacient" do
      get :new
      expect(assigns(:pacient)).to be_a_new(Pacient)
    end

    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new contact in the database" do
        expect{
          post :create, pacient: attributes_for(:pacient)
        }.to change(Pacient, :count).by(1)
      end

      it "redirects to the root" do
        post :create, pacient: attributes_for(:pacient)
        expect(response).to redirect_to root_path(assigns[:pacient])
      end
    end

    context "with invalid attributes" do 
      it "does not save the new contact in the database" do 
        expect{
          post :create,
            pacient: attributes_for(:invalid_pacient)
        }.to_not change(Pacient, :count)
      end

      it "re-renders the :new template" do
        post :create, pacient: attributes_for(:invalid_pacient)
        expect(response).to render_template :new
      end

    end
  end

  describe "GET #edit" do
    it "assigns the requested contact to @contact" do
      pacient = create(:pacient, user_id: @user.id)
      get :edit, id: pacient.id
      expect(assigns(:pacient)).to eq pacient
    end

    it "renders the :edit template" do
      pacient = create(:pacient, user_id: @user.id)
      get :edit, id: pacient.id
      expect(response).to render_template :edit
    end
  end

  describe "PATCH #update" do
    before :each do
      @pac = create(:pacient, name: "Isaias", user_id: @user.id)
    end

    context "with valid attributes" do
      it "locates the requested @pacient" do
        patch :update, id: @pac, pacient: attributes_for(:pacient)
        expect(assigns(:pacient)).to eq(@pac)
      end

      it "changes @pac's attributes" do
        patch :update, id: @pac, 
          pacient: attributes_for(:pacient, name: "Isaias", user_id: @user.id)
        @pac.reload
        expect(@pac.name).to eq("Isaias")
      end

      it "redirects to the updated pacient to root" do
        patch :update, id: @pac, pacient: attributes_for(:pacient)
        expect(response).to redirect_to root_path
      end
    end

    context "with invalid attributes" do
      it "does not update the pacient's attributes" do
        patch :update, id: @pac,
         pacient: attributes_for(:pacient, name: "Hugo", birth: nil, user_id: @user.id)
        @pac.reload
        expect(@pac.name).to_not eq("Hugo")
      end

      it "re-renders the :edit template" do
        patch :update, id: @pac, pacient: attributes_for(:invalid_pacient)
        expect(response).to render_template :edit
      end
    end
  end

  describe "DELETE #destroy" do
    before :each do
      @pac = create(:pacient, user_id: @user.id)
    end

    it "deletes the pacient" do
      expect{
        delete :destroy, id: @pac
      }.to change(Pacient, :count).by(-1)
    end

    it "redirects to root" do
      delete :destroy, id: @pac
      expect(response).to redirect_to root_path
    end
  end

end