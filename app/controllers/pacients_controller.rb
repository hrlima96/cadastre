class PacientsController < ApplicationController

  def index
    @pacients = Pacient.all
  end

  def new
    @pacient = current_user.pacients.build
  end

  def show
    @pacient = current_user.pacients.find(params[:id])
  end

  def edit
    @pacient = current_user.pacients.find(params[:id])
  end

  def create
    @pacient = current_user.pacients.build(pacient_params)
    if @pacient.save
      redirect_to root_path, notice: "Paciente criado com sucesso."
    else
      render :new
    end
  end

  def update
    @pacient = current_user.pacients.find(params[:id])
    if @pacient.update_attributes(pacient_params)
      redirect_to root_path, notice: "Paciente atualizado com sucesso."
    else
      render :edit
    end
  end

  def destroy
    @pacient = current_user.pacients.find(params[:id])
    @pacient.destroy

    redirect_to root_path, notice: "Paciente excluido com sucesso."
  end

  private

  def pacient_params
    params.require(:pacient).permit(:name, :birth, :dependents, :cpf, :picture)
  end
end
