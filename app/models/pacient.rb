require 'carrierwave/orm/activerecord'

class Pacient < ActiveRecord::Base
  validates :name, presence: true
  validates :birth, presence: true
  validates :dependents, presence: true, inclusion: {in: (0..50)}
  validates :cpf, uniqueness: true
  validate :validate_cpf

  belongs_to :user

  mount_uploader :picture, ::PictureUploader

  def validate_cpf
    if cpf
      lista_cpf = cpf.gsub('.' ,'').gsub('-', '').split('')
      if lista_cpf.length == 11
        soma = 0
        mult = 10

        for i in (0..lista_cpf.length - 3)
          soma += lista_cpf[i].to_i * mult
          mult -= 1
        end

        mod = soma % 11

        if mod == 0 or mod == 1
          validator1 = 0
        else
          validator1 = 11 - mod
        end

        soma = 0
        mult = 11

        for i in (0..lista_cpf.length - 2)
          soma += lista_cpf[i].to_i * mult
          mult -= 1
        end

        mod = soma % 11

        if mod == 0 or mod == 1
          validator2 = 0
        else
          validator2 = 11 - mod
        end

        unless cpf[-2].to_i == validator1 and cpf[-1].to_i == validator2
          errors.add(:cpf, "Cpf inválido!")
        end

      else
        errors.add(:cpf, "Cpf de comprimento inválido!")
      end

    else
      errors.add(:cpf, "Can't be blank")
    end
  end
end
